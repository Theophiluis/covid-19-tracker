package ggs.co.za.crimap.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.globals.FrameworkActivity;

public class WebActivity extends AppCompatActivity {

    WebView webView;
    private AppCompatActivity appCompatActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        appCompatActivity = this;

        FrameworkActivity.manageToolbarBack(appCompatActivity);

        webView = (WebView) findViewById(R.id.webView);


        webView.loadUrl(getIntent().getExtras().getString("link").replace("http://", "https://"));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                ((ProgressBar)findViewById(R.id.loadingView)).setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                ((ProgressBar)findViewById(R.id.loadingView)).setVisibility(View.GONE);
            }
        });

    }
}
