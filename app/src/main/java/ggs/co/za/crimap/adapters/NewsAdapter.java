package ggs.co.za.crimap.adapters;

import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.prof.rssparser.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.globals.DateHelper;
import ggs.co.za.crimap.globals.StringHelper;

public class NewsAdapter extends BaseAdapter {


    private AppCompatActivity appCompatActivity;
    private List<Article> items;

    public NewsAdapter (AppCompatActivity appCompatActivity, List<Article> items) {
        this.appCompatActivity = appCompatActivity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = appCompatActivity.getLayoutInflater().inflate(R.layout.list_news_item, null);
        }

        Article item = items.get(i);

        ((TextView)view.findViewById(R.id.title)).setText(item.getTitle());
        ((TextView)view.findViewById(R.id.description)).setText(Html.fromHtml(StringHelper.html2text(item.getDescription())));
        ((TextView)view.findViewById(R.id.pubDate)).setText(DateHelper.getTimeAgo(DateHelper.getLongFromString(item.getPubDate())));
        if (item.getImage() != null && !item.getImage().equalsIgnoreCase("null")) {
            ((ImageView)view.findViewById(R.id.image)).setVisibility(View.VISIBLE);
            Glide.with(appCompatActivity)
                    .load(item.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(((ImageView)view.findViewById(R.id.image)));
        } else {
            ((ImageView)view.findViewById(R.id.image)).setVisibility(View.GONE);
        }

        return view;
    }
}
