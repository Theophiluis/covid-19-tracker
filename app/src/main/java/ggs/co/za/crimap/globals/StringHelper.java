package ggs.co.za.crimap.globals;

import org.jsoup.Jsoup;

public class StringHelper {

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }
}
