package ggs.co.za.crimap.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import ggs.co.za.crimap.fragments.CountriesFragment;
import ggs.co.za.crimap.fragments.GlobalFragment;
import ggs.co.za.crimap.fragments.NewFragment;
import ggs.co.za.crimap.fragments.StatisticsFragment;

public class HomePagerAdapter extends FragmentStatePagerAdapter {


    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new GlobalFragment();
        }
        if (position == 1) {
            return new CountriesFragment();
        }
        if (position == 2) {
            return new NewFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Global";
        }
        if (position == 1) {
            return "Countries";

        }
        if (position == 2) {
            return "News";
        }

        return "";
    }
}
