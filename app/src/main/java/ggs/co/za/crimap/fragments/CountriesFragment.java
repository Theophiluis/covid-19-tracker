package ggs.co.za.crimap.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.activities.CountryActivity;
import ggs.co.za.crimap.adapters.CountryListAdapter;
import ggs.co.za.crimap.connections.ConnectionModel;
import ggs.co.za.crimap.globals.SortingHelper;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CountriesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CountriesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CountriesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CountriesFragment() {
        // Required empty public constructor
    }

    private View view;
    private CountryListAdapter countryListAdapter;

    private JSONArray globalData;
    private List<JSONObject> jsonValues;
    private List<JSONObject> updatedList;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CountriesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CountriesFragment newInstance(String param1, String param2) {
        CountriesFragment fragment = new CountriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_countries, container, false);
        countryListAdapter = new CountryListAdapter((AppCompatActivity) getActivity());
        getCountriesArray();
        return view;
    }

    private void getCountriesArray() {
        ConnectionModel.GET(getContext(), getString(R.string.countries_ep), new ConnectionModel.VolleyCallbackGET() {
            @Override
            public void onSuccess(String result, VolleyError error) {
                ((ProgressBar) view.findViewById(R.id.loadingView)).setVisibility(View.GONE);
                if (result != null) {
                    try {
                        globalData = new JSONArray(result);
                        jsonValues = new ArrayList<JSONObject>();
                        for (int i = 0; i < globalData.length(); i++) {
                            if (!globalData.getJSONObject(i).getString("country").equalsIgnoreCase("")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("total")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("world")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().equalsIgnoreCase("africa")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("asia")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("europe")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("north america")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("south america")
                                    && !globalData.getJSONObject(i).getString("country").toLowerCase().contains("oceania")) {
//                                if (i == 3 || i == 10 || i == 18) {
//                                    jsonValues.add(new JSONObject().put("isAd", ""));
//                                }
                                jsonValues.add(globalData.getJSONObject(i));
                            }
                        }

                        SortingHelper.sortList(jsonValues, "cases");
                        updatedList = jsonValues;

                        countryListAdapter.setItems(jsonValues);
                        ((ListView) view.findViewById(R.id.listView)).setAdapter(countryListAdapter);
                        ((ListView) view.findViewById(R.id.listView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                Intent intent = new Intent(getActivity(), CountryActivity.class);
                                intent.putExtra("country", updatedList.get(i).toString());
                                startActivity(intent);
                            }
                        });

                        ((EditText)view.findViewById(R.id.searchText)).addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                updatedList = new ArrayList<>();
                                for (int i = 0; i < jsonValues.size(); i++) {
                                    try {
                                        if (jsonValues.get(i).getString("country").toLowerCase().contains(editable.toString().toLowerCase())) {
                                            updatedList.add(jsonValues.get(i));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (updatedList.size() == 0) {
                                    updatedList = jsonValues;
                                }
                                countryListAdapter.setItems(updatedList);
                                countryListAdapter.notifyDataSetChanged();
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }, new HashMap<String, String>());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
