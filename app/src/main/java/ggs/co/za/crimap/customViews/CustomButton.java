package ggs.co.za.crimap.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;


/**
 * Created by jzeltheophiluis on 2018/02/06.
 */

public class CustomButton extends AppCompatButton {


    private static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    private Context context;


    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init(attrs);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public CustomButton(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public void init(AttributeSet attributeSet) {
        applyCustomFont(context, attributeSet);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

            Typeface customFont = selectTypeface(context, textStyle);
            setTypeface(customFont);
        }
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        /*
        * information about the TextView textStyle:
        * http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
        */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface("font/font_bold.ttf", context);
            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("font/font_light.ttf", context);
            case Typeface.BOLD_ITALIC: // bold italic
                return FontCache.getTypeface("font/font_bold.ttf", context);
            case Typeface.NORMAL: // regular
                return FontCache.getTypeface("font/font_light.ttf", context);
            default:
                return FontCache.getTypeface("font/font_light.ttf", context);
        }
    }
}
