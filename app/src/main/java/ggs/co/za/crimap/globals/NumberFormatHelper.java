package ggs.co.za.crimap.globals;

import java.text.DecimalFormat;

public class NumberFormatHelper {

    public static String FormatNumberForMil (int number) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(number);
    }
}
