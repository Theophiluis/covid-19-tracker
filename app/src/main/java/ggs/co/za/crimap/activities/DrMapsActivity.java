package ggs.co.za.crimap.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.LocationBias;
import com.google.android.libraries.places.api.model.LocationRestriction;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.adapters.DoctorsAdapter;
import ggs.co.za.crimap.adapters.DoctorsPagerAdapter;
import ggs.co.za.crimap.connections.ConnectionModel;
import ggs.co.za.crimap.globals.FrameworkActivity;

public class DrMapsActivity extends FragmentActivity implements OnMapReadyCallback,
        LocationListener,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener {

    private GoogleMap mMap;
    private FragmentActivity appCompatActivity;
    private LocationManager locationManager;
    private Location location;


    private Context mContext;
    private JSONArray resultsList;
    private PlacesClient placesClient;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private HashMap<Marker, Integer> markerMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_maps);
        appCompatActivity = this;
        mContext = this;


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        placesClient = Places.createClient(this);
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.setMyLocationEnabled(true);
        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_json));

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mMap != null) {
            this.location = location;
            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(location.getLatitude(), location.getLongitude())));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13);
            mMap.animateCamera(cameraUpdate);
            locationManager.removeUpdates(this);


        }
    }

    private void getDoctors() {
        ((ProgressBar)findViewById(R.id.loadingView)).setVisibility(View.VISIBLE);
        String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=doctor&inputtype=textquery&rankBy=distance&fields=photos,formatted_phone_number,formatted_address,name,opening_hours,rating" +
                "&radius=2000&location=" +
                mMap.getCameraPosition().target.latitude + "," + mMap.getCameraPosition().target.longitude + "&key=AIzaSyApAOwu2QVoFZgcPmBFgG3BQ2qQwYre09Y";

        ConnectionModel.GET(getApplicationContext(), url, new ConnectionModel.VolleyCallbackGET() {
            @Override
            public void onSuccess(String result, VolleyError error) {
                ((ProgressBar)findViewById(R.id.loadingView)).setVisibility(View.GONE);
                if (result != null) {
                    try {
                        resultsList = new JSONObject(result).getJSONArray("results");
                        handleMapMarkers(0);


                        ((ViewPager)findViewById(R.id.viewPager)).setOffscreenPageLimit(20);
                        ((ViewPager)findViewById(R.id.viewPager)).setAdapter(new DoctorsPagerAdapter(appCompatActivity, resultsList));
                        ((ViewPager)findViewById(R.id.viewPager)).addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                handleMapMarkers(position);
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

//                        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

                        // use this setting to improve performance if you know that changes
                        // in content do not change the layout size of the RecyclerView
//                        recyclerView.setHasFixedSize(true);
                        // use a linear layout manager
//                        layoutManager = new LinearLayoutManager(getApplicationContext());
//                        recyclerView.setLayoutManager(layoutManager);

                        // specify an adapter (see also next example)
//                        mAdapter = new DoctorsAdapter(appCompatActivity, resultsList);
//                        recyclerView.setAdapter(mAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }, new HashMap<String, String>());

    }

    private void handleMapMarkers(int index) {
        mMap.clear();
        markerMap = new HashMap<>();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < resultsList.length(); i++) {
            try {
                JSONObject geometry = resultsList.getJSONObject(i).getJSONObject("geometry");
                LatLng latLng =  new LatLng(geometry.getJSONObject("location").getDouble("lat"),geometry.getJSONObject("location").getDouble("lng"));
                builder.include(latLng);
                if (index == i) {
//                    mMap.addMarker(new MarkerOptions().position(latLng)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.doctor_coat));
                    //Skip, add this last to be on top
                } else {
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.doctor))
                            .position(latLng));
                    marker.setTag(i);
                    markerMap.put(marker, i);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject geometry = null;
        try {
            geometry = resultsList.getJSONObject(index).getJSONObject("geometry");
            LatLng latLng =  new LatLng(geometry.getJSONObject("location").getDouble("lat"),geometry.getJSONObject("location").getDouble("lng"));
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .zIndex(1000)
                    .position(latLng));
            marker.setTag(index);
            markerMap.put(marker, index);
        } catch (JSONException e) {
            e.printStackTrace();
        }



//        LatLngBounds bounds = builder.build();
//        int padding = 20; // offset from edges of the map in pixels
//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//        mMap.animateCamera(cu);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onCameraIdle() {
        Log.w("", "");
        getDoctors();
    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveCanceled() {
        Log.w("", "");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        ((ViewPager)findViewById(R.id.viewPager)).setCurrentItem(markerMap.get(marker));
//        handleMapMarkers(markerMap.get(marker));
        return true;
    }
}
