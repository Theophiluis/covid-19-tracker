package ggs.co.za.crimap.adapters;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ggs.co.za.crimap.R;

public class DoctorsPagerAdapter extends PagerAdapter {

    private FragmentActivity activity;
    private JSONArray items;

    public DoctorsPagerAdapter (FragmentActivity activity, JSONArray items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final View itemView = activity.getLayoutInflater().inflate(R.layout.doctor_view, container, false);
        try {
            JSONObject item = items.getJSONObject(position);
            ((TextView)itemView.findViewById(R.id.nameText)).setText(item.getString("name"));
            ((TextView)itemView.findViewById(R.id.address)).setText(item.getString("formatted_address"));

            if (item.has("rating")) {
                ((RatingBar)itemView.findViewById(R.id.rating)).setRating((float) item.getDouble("rating"));
            }

            if (item.has("user_ratings_total")) {
                ((TextView)itemView.findViewById(R.id.totalRatings)).setText(item.getString("user_ratings_total"));
            }


            if (item.has("photos")) {
                final String url = "https://maps.googleapis.com/maps/api/place/photo?" +
                        "maxwidth=300&" +
                        "photoreference=" +
                        item.getJSONArray("photos").getJSONObject(0).getString("photo_reference") +
                        "&key=" + activity.getString(R.string.api_key_pub);
                Glide.with(activity)
                        .load(Uri.parse(url))
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .listener(new RequestListener<Uri, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                                Log.w("IMAGE URL:", url);
                                Log.w("IMAGE Error:", e);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                Log.w("IMAGE Ready:", url);
                                ((ImageView)itemView.findViewById(R.id.doctorImage)).setImageDrawable(resource);
                                return true;
                            }
                        })
                        .into(((ImageView)itemView.findViewById(R.id.doctorImage)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
