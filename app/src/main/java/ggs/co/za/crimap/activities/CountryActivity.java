package ggs.co.za.crimap.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.globals.CalculatorHelper;
import ggs.co.za.crimap.globals.FrameworkActivity;
import ggs.co.za.crimap.globals.NumberFormatHelper;

public class CountryActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    private AppCompatActivity appCompatActivity;
    private JSONObject country;
    private PieChart chart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        appCompatActivity = this;
        chart = (PieChart)findViewById(R.id.chart1);

        try {
            country = new JSONObject(getIntent().getStringExtra("country"));

            ((TextView)findViewById(R.id.countryName)).setText(country.getString("country"));
            if (!country.isNull("cases")) {
                ((TextView)findViewById(R.id.casesText)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("cases")));
            }
            if (!country.isNull("todayCases")) {
                ((TextView) findViewById(R.id.newCases)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("todayCases")));
            }
            if (!country.isNull("todayDeaths")) {
                ((TextView) findViewById(R.id.newDeaths)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("todayDeaths")));
            }

            if (!country.isNull("active")) {
                String activeParaHtml = "<font color=\'#b1b1b1\'>There are </font><font color=\'#475C6D\'>"
                        + NumberFormatHelper.FormatNumberForMil((Integer) country.get("active")) + "</font><font color=\'#b1b1b1\'> active cases </font>";
                ((TextView) findViewById(R.id.activeText)).setText(Html.fromHtml(activeParaHtml));
            }

            if (!country.isNull("deaths")) {
                ((TextView) findViewById(R.id.deathsText)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("deaths")));
            }
            if (!country.isNull("recovered")) {
                ((TextView) findViewById(R.id.recoveredText)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("recovered")));
            }
            if (!country.isNull("critical")) {
                ((TextView) findViewById(R.id.criticalCases)).setText(NumberFormatHelper.FormatNumberForMil(country.getInt("critical")));
            }

            double deathPerc = 0;
            double recoveryPerc = 0;

            if (!country.isNull("deaths")) {
                deathPerc = (Double.parseDouble(country.getString("deaths")) / Double.parseDouble(country.getString("cases"))) * 100;
            }
            if (!country.isNull("recovered")) {
                ((TextView)findViewById(R.id.recoveryRatio)).setText("" + CalculatorHelper.GetDeathToRecoveryRatio(country.getInt("deaths"), country.getInt("recovered")));
                recoveryPerc = (Double.parseDouble(country.getString("recovered")) / Double.parseDouble(country.getString("cases"))) * 100;
            }

            String percParaHtml = "<font color=\'#b1b1b1\'>The current recovery rate is </font><font color=\'#A8E687\'><b>"
                    + String.format("%.2f", recoveryPerc)
                    + "%</b></font><font color=\'#b1b1b1\'>, while the death rate is </font>"
                    +"<font color=\'#E68787\'><b>"
                    + String.format("%.2f", deathPerc)
                    + "%</b></font>";

            ((TextView)findViewById(R.id.percentageText)).setText(Html.fromHtml(percParaHtml));

            setOrdersChartData();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        manageToolbar();
    }


    private void setOrdersChartData() {
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(-10, 0, -10, -80);
        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);
        chart.getLegend().setEnabled(false);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setMaxAngle(180f);
        chart.setRotationAngle(180f);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(false);

        // enable rotation of the chart by touch
        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        chart.animateY(1400, Easing.EaseInOutQuad);
        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
//        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);


        ArrayList<PieEntry> entries = new ArrayList<>();
        try {
            entries.add(new PieEntry(country.getInt("recovered")));
            entries.add(new PieEntry(country.getInt("critical")));
            entries.add(new PieEntry(country.getInt("deaths")));


            PieDataSet dataSet = new PieDataSet(entries, "");
            dataSet.setDrawIcons(false);

            dataSet.setSliceSpace(3f);
            dataSet.setSelectionShift(5f);
            ArrayList<Integer> colors = new ArrayList<>();
            colors.add(getResources().getColor(R.color.greenColor));
            colors.add(getResources().getColor(R.color.orangeColor));
            colors.add(getResources().getColor(R.color.redColor));


            dataSet.setColors(colors);
            //dataSet.setSelectionShift(0f);

            PieData data = new PieData(dataSet);
            data.setValueFormatter(new PercentFormatter(chart));
            data.setValueTextSize(0f);

            data.setValueTextColor(Color.TRANSPARENT);
//            data.setValueTypeface(tfLight);
            chart.setData(data);

            // undo all highlights
            chart.highlightValues(null);

            chart.invalidate();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void manageToolbar() {
        FrameworkActivity.manageToolbarBack(
                appCompatActivity
        );
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
