package ggs.co.za.crimap.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.activities.DrMapsActivity;
import ggs.co.za.crimap.connections.ConnectionModel;
import ggs.co.za.crimap.globals.CalculatorHelper;
import ggs.co.za.crimap.globals.NumberFormatHelper;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GlobalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GlobalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GlobalFragment extends Fragment implements OnChartValueSelectedListener, LocationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public GlobalFragment() {
        // Required empty public constructor
    }

    private View view;
    private PieChart chart;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GlobalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GlobalFragment newInstance(String param1, String param2) {
        GlobalFragment fragment = new GlobalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private LocationManager locationManager;
    private Location location;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_global, container, false);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);

        ((Button)view.findViewById(R.id.showingSymptoms)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DrMapsActivity.class);
                startActivity(intent);
            }
        });

        getGlobalData();
        return view;
    }

    private void getGlobalData() {
        ConnectionModel.GET(getContext(), getString(R.string.countries_ep), new ConnectionModel.VolleyCallbackGET() {
            @Override
            public void onSuccess(String result, VolleyError error) {
                ((ProgressBar)view.findViewById(R.id.loadingView)).setVisibility(View.GONE);
                ((ScrollView)view.findViewById(R.id.scrollView)).setVisibility(View.VISIBLE);
                if (result != null) {
                    try {
                        int total = 0;
                        int deaths = 0;
                        int recovered = 0;
                        int active = 0;
                        JSONArray countries = new JSONArray(result);
                        for (int i = 0;i < countries.length(); i++) {
                            if (!countries.getJSONObject(i).getString("country").equalsIgnoreCase("")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("total")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("world")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().equalsIgnoreCase("africa")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("asia")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("europe")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("north america")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("south america")
                                    && !countries.getJSONObject(i).getString("country").toLowerCase().contains("oceania")) {
                                if (!countries.getJSONObject(i).isNull("cases")) {
                                    total += countries.getJSONObject(i).getInt("cases");
                                }
                                if (!countries.getJSONObject(i).isNull("deaths")) {
                                    deaths += countries.getJSONObject(i).getInt("deaths");
                                }
                                if (!countries.getJSONObject(i).isNull("recovered")) {
                                    recovered += countries.getJSONObject(i).getInt("recovered");
                                }
                                if (!countries.getJSONObject(i).isNull("active")) {
                                    active += countries.getJSONObject(i).getInt("active");
                                }
                            }
                        }


                        String casesFormatted = NumberFormatHelper.FormatNumberForMil(total);
                        String deathsFormatted =   NumberFormatHelper.FormatNumberForMil(deaths);
                        String recoveredFormatted =  NumberFormatHelper.FormatNumberForMil(recovered);
                        String activeFormatted =  NumberFormatHelper.FormatNumberForMil(active);


                        ((TextView)view.findViewById(R.id.recoveryRatio)).setText("" + CalculatorHelper.GetDeathToRecoveryRatio(deaths, recovered));
                        ((TextView)view.findViewById(R.id.totalText)).setText(casesFormatted);
                        ((TextView)view.findViewById(R.id.deathsText)).setText(deathsFormatted );
                        ((TextView)view.findViewById(R.id.recoveredText)).setText(recoveredFormatted);
                        ((TextView)view.findViewById(R.id.activeText)).setText(activeFormatted);
                        ((LinearLayout)view.findViewById(R.id.mainView)).setVisibility(View.VISIBLE);

                        setOrdersChartData(total, deaths, recovered, active);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }, new HashMap<String, String>());
    }

    private void setOrdersChartData(int total, int deaths, int recovered, int active) {
        chart = view.findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);


        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(getResources().getColor(R.color.colorPrimary));
        chart.getLegend().setEnabled(false);



        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        chart.animateY(1400, Easing.EaseInOutQuad);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
//        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);


        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(recovered));
        entries.add(new PieEntry(active));
        entries.add(new PieEntry(deaths));

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.greenColor));
        colors.add(getResources().getColor(R.color.orangeColor));
        colors.add(getResources().getColor(R.color.redColor));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(12f);
        data.setValueTextColor(getResources().getColor(R.color.colorPrimaryDarkDark));
//            data.setValueTypeface(tfLight);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }

    private void setOrdersChartData(JSONObject globalData) {
        chart = view.findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);


        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(getResources().getColor(R.color.colorPrimary));
        chart.getLegend().setEnabled(false);



        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);



        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);


        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
//        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);


        ArrayList<PieEntry> entries = new ArrayList<>();
        try {
            entries.add(new PieEntry(globalData.getInt("deaths")));
            entries.add(new PieEntry(globalData.getInt("recovered")));

            PieDataSet dataSet = new PieDataSet(entries, "");
            dataSet.setDrawIcons(false);

            dataSet.setSliceSpace(3f);
            dataSet.setSelectionShift(5f);
            ArrayList<Integer> colors = new ArrayList<>();
            colors.add(getResources().getColor(R.color.redColor));
            colors.add(getResources().getColor(R.color.greenColor));

            dataSet.setColors(colors);
            //dataSet.setSelectionShift(0f);

            PieData data = new PieData(dataSet);
            data.setValueFormatter(new PercentFormatter(chart));
            data.setValueTextSize(12f);
            data.setValueTextColor(getResources().getColor(R.color.colorPrimaryDarkDark));
//            data.setValueTypeface(tfLight);
            chart.setData(data);

            // undo all highlights
            chart.highlightValues(null);

            chart.invalidate();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onLocationChanged(Location location) {
        locationManager.removeUpdates(this);
//        AdView mAdView = view.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder()
//                .setLocation(location)
//                .build();
//        mAdView.loadAd(adRequest);
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                // Code to be executed when an ad finishes loading.
//                Log.w("Ad Loaded", "");
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                // Code to be executed when an ad request fails.
//                Log.w("Ad failed to load", "" + errorCode);
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when an ad opens an overlay that
//                // covers the screen.
//            }
//
//            @Override
//            public void onAdClicked() {
//                // Code to be executed when the user clicks on an ad.
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when the user is about to return
//                // to the app after tapping on an ad.
//            }
//        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
