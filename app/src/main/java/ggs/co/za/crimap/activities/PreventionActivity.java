package ggs.co.za.crimap.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.globals.FrameworkActivity;

public class PreventionActivity extends AppCompatActivity {

    private AppCompatActivity appCompatActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prevention);
        appCompatActivity = this;

        FrameworkActivity.manageToolbarBack(appCompatActivity);
    }
}
