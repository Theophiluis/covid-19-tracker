package ggs.co.za.crimap.globals;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingHelper {

    public static void sortList(List<JSONObject> jsonValues, final String key) {
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {
                int valA =  0;
                int valB =  0;

                try {
                    if (a.has("cases")) {
                        valA = (int) a.get(key);
                    }
                    if (b.has("cases")) {
                        valB = (int) b.get(key);
                    }
                }
                catch (JSONException e) {
                    Log.e("", "JSONException in combineJSONArrays sort section", e);
                }

                if (valA > valB) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
    }
}
