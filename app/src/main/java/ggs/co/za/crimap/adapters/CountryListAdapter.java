package ggs.co.za.crimap.adapters;

import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.globals.NumberFormatHelper;

public class CountryListAdapter extends BaseAdapter {

    private AppCompatActivity appCompatActivity;
    private List<JSONObject> items;


    public CountryListAdapter(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        try {
            JSONObject item = items.get(i);
//            if (item.has("isAd")) {
//                if (view == null) {
//                    view = appCompatActivity.getLayoutInflater().inflate(R.layout.list_country_ad_item, null);
//                }
//                ((AdView)view.findViewById(R.id.adView)).setAdSize(AdSize.LARGE_BANNER);
//            } else {
                if (view == null) {
                    view = appCompatActivity.getLayoutInflater().inflate(R.layout.list_country_item, null);
                }
                ((TextView) view.findViewById(R.id.countryName)).setText(item.getString("country"));
                ((TextView) view.findViewById(R.id.casesText)).setText(NumberFormatHelper.FormatNumberForMil((Integer) item.get("cases")));


                if (!item.isNull("deaths")) {
                    String htmlDeaths = "<font color=\'#ffffff\'>Deaths: </font><font color=\'#E68787\'>" + NumberFormatHelper.FormatNumberForMil((Integer) item.get("deaths")) + "</font>";
                    ((TextView) view.findViewById(R.id.deathsText)).setText(Html.fromHtml(htmlDeaths));
                    ((TextView) view.findViewById(R.id.deathsTextMain)).setText(NumberFormatHelper.FormatNumberForMil((Integer) item.get("deaths")));
                } else {
                    ((TextView) view.findViewById(R.id.deathsText)).setText("N/A");
                    ((TextView) view.findViewById(R.id.deathsTextMain)).setText("N/A");
                }

                if (!item.isNull("recovered")) {
                    String htmlrecovered = "<font color=\'#ffffff\'>Recovered: </font><font color=\'#A8E687\'>" + NumberFormatHelper.FormatNumberForMil((Integer) item.get("recovered")) + "</font>";
                    ((TextView) view.findViewById(R.id.recoveredText)).setText(Html.fromHtml(htmlrecovered));
                    ((TextView) view.findViewById(R.id.recoveredTextMain)).setText(NumberFormatHelper.FormatNumberForMil((Integer) item.get("recovered")));
                } else {
                    ((TextView) view.findViewById(R.id.recoveredText)).setText("N/A");
                    ((TextView) view.findViewById(R.id.recoveredTextMain)).setText("N/A");
                }

                if (!item.isNull("active")) {
                    String htmlactive = "<font color=\'#ffffff\'>Active: </font><font color=\'#DDDDDD\'>" + NumberFormatHelper.FormatNumberForMil((Integer) item.get("active")) + "</font>";
                    ((TextView) view.findViewById(R.id.activeText)).setText(Html.fromHtml(htmlactive));
                    String activeParaHtml = "<font color=\'#ffffff\'></font><font color=\'#DDDDDD\'>"
                            + NumberFormatHelper.FormatNumberForMil((Integer) item.get("active")) + "</font><font color=\'#ffffff\'> active cases </font>";
                    ((TextView) view.findViewById(R.id.activeTextPara)).setText(Html.fromHtml(activeParaHtml));
                } else {
                    ((TextView) view.findViewById(R.id.activeText)).setText("N/A");
                    ((TextView) view.findViewById(R.id.activeTextPara)).setText("N/A");
                }

                if (!item.isNull("critical")) {
                    String htmlCritical = "<font color=\'#ffffff\'>Critical: </font><font color=\'#DDDDDD\'>" + NumberFormatHelper.FormatNumberForMil((Integer) item.get("critical")) + "</font>";
                    ((TextView) view.findViewById(R.id.criticalText)).setText(Html.fromHtml(htmlCritical));
                } else {
                    ((TextView) view.findViewById(R.id.criticalText)).setText("N/A");
                    ((TextView) view.findViewById(R.id.criticalText)).setText("N/A");
                }
//            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }

    public void setItems(List<JSONObject> items) {
        this.items = items;
    }
}
