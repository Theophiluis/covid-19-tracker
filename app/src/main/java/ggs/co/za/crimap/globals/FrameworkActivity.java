package ggs.co.za.crimap.globals;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import ggs.co.za.crimap.R;

public class FrameworkActivity {


    public static void manageToolbarBack (final AppCompatActivity activity) {
        if (activity != null) {
            Toolbar toolbar = activity.findViewById(R.id.toolbar);
            activity.setSupportActionBar(toolbar);

            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);


            // Show the Up button in the action bar.
            final Drawable upArrow = activity.getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
//            final Drawable upArrow = activity.getResources().getDrawable(R.drawable.abc_ic_ab_back_material_google);
            upArrow.setColorFilter(activity.getResources().getColor(R.color.arrowColor), PorterDuff.Mode.SRC_ATOP);
            activity.getSupportActionBar().setHomeAsUpIndicator(upArrow);
            // Show the Up button in the action bar.
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //What to do on back clicked
                    activity.finish();
                }
            });

        }
    }
}
