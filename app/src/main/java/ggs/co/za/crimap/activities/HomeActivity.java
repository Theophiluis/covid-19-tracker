package ggs.co.za.crimap.activities;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.adapters.HomePagerAdapter;
import ggs.co.za.crimap.connections.ConnectionModel;
import ggs.co.za.crimap.fragments.CountriesFragment;
import ggs.co.za.crimap.fragments.GlobalFragment;
import ggs.co.za.crimap.fragments.NewFragment;
import ggs.co.za.crimap.fragments.StatisticsFragment;

public class HomeActivity extends AppCompatActivity implements GlobalFragment.OnFragmentInteractionListener,
        CountriesFragment.OnFragmentInteractionListener,
        StatisticsFragment.OnFragmentInteractionListener,
        NewFragment.OnFragmentInteractionListener {

    private Context mContext;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private AppCompatActivity appCompatActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        appCompatActivity = this;

        mContext = this;
        ((ViewPager) findViewById(R.id.pager)).setAdapter(new HomePagerAdapter(getSupportFragmentManager()));
        ((ViewPager) findViewById(R.id.pager)).setOffscreenPageLimit(5);
        ((TabLayout) findViewById(R.id.tab_layout)).setupWithViewPager(((ViewPager) findViewById(R.id.pager)));
//        ((TabLayout) findViewById(R.id.tab_layout)).setTabTextColors(getResources().getColor(R.color.colorAccent), Color.WHITE);

        ((Button) findViewById(R.id.whatIsBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(appCompatActivity, WhatIsCovid.class);
                startActivity(i);
            }
        });

        ((Button) findViewById(R.id.symptoms)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(appCompatActivity, SymptomsActivity.class);
                startActivity(i);
            }
        });

        ((Button) findViewById(R.id.prevention)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(appCompatActivity, PreventionActivity.class);
                startActivity(i);
            }
        });

        ((Button) findViewById(R.id.treatment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(appCompatActivity, TreatmentActivity.class);
                startActivity(i);
            }
        });

        ((ImageView) findViewById(R.id.ggs_image)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(appCompatActivity, WebActivity.class);
                intent.putExtra("link", "https://galacticgroupsolutions.co.za/");
                startActivity(intent);
            }
        });

        ((RelativeLayout)findViewById(R.id.mainMenuView)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        setDrawer();
    }


    private void setDrawer() {
        DrawerLayout drawer;

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(appCompatActivity, drawer, (Toolbar) findViewById(R.id.toolbar), R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
