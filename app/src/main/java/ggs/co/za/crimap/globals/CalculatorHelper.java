package ggs.co.za.crimap.globals;

import java.util.ArrayList;
import java.util.List;

public class CalculatorHelper {

    public static int GetDeathToRecoveryRatio (float deaths, float recoveries) {
        float ratioRec  = recoveries / deaths;
        return Math.round(ratioRec);
    }
}
