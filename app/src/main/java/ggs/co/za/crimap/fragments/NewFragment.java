package ggs.co.za.crimap.fragments;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prof.rssparser.Article;
import com.prof.rssparser.Channel;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ggs.co.za.crimap.R;
import ggs.co.za.crimap.activities.WebActivity;
import ggs.co.za.crimap.adapters.NewsAdapter;
import ggs.co.za.crimap.connections.ConnectionModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewFragment extends Fragment implements LocationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewFragment newInstance(String param1, String param2) {
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private LocationManager locationManager;
    private Location location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_new, container, false);
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
        return view;
    }

    private void getNews() {
//        ConnectionModel.GET(getActivity(), getString(R.string.news_ep) + "&apiKey=" + getString(R.string.news_api_key), new ConnectionModel.VolleyCallbackGET() {
//            @Override
//            public void onSuccess(String result, VolleyError error) {
//                ((ProgressBar)view.findViewById(R.id.loadingView)).setVisibility(View.GONE);
//                if (result != null) {
//                    try {
//                        final JSONArray articles = new JSONArray(new JSONObject(result).getString("articles"));
//                        ((ListView)view.findViewById(R.id.listView)).setAdapter(new NewsAdapter((AppCompatActivity) getActivity(), articles));
//                        ((ListView)view.findViewById(R.id.listView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                try {
//                                    JSONObject article = articles.getJSONObject(i);
//                                    Intent intent = new Intent(getActivity(), WebActivity.class);
//                                    intent.putExtra("link", article.getString("url"));
//                                    startActivity(intent);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        });
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//
//                }
//            }
//        }, new HashMap<String, String>());

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only


//            ConnectionModel.GET(getActivity(), getString(R.string.news_google_rss) +
//                    "hl=en-" + addresses.get(0).getCountryCode() + "&gl=" + addresses.get(0).getCountryCode() + "&ceid=" + "NG" + ":en", new ConnectionModel.VolleyCallbackGET() {
//                @Override
//                public void onSuccess(String result, VolleyError error) {
//                    if (result != null) {
//
//                    }
//                }
//            }, new HashMap<String, String>());

            String urlString = getString(R.string.news_google_rss) +
                    "hl=en-" + addresses.get(0).getCountryCode()
                    + "&q=COVID-19"
                    + "&gl="
                    + addresses.get(0).getCountryCode()
                    + "&ceid="
                    + addresses.get(0).getCountryCode()
                    + ":en";
            Parser parser = new Parser();
            parser.onFinish(new OnTaskCompleted() {

                //what to do when the parsing is done
                @Override
                public void onTaskCompleted(final Channel channel) {
                    // Use the channel info
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((ProgressBar) view.findViewById(R.id.loadingView)).setVisibility(View.GONE);
                            ((ListView)view.findViewById(R.id.listView)).setAdapter(new NewsAdapter((AppCompatActivity) getActivity(), channel.getArticles()));
                            ((ListView)view.findViewById(R.id.listView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Article article = channel.getArticles().get(i);
                                    Intent intent = new Intent(getActivity(), WebActivity.class);
                                    intent.putExtra("link", article.getLink());
                                    startActivity(intent);

                                }
                            });
                        }
                    });
                }

                //what to do in case of error
                @Override
                public void onError(Exception e) {
                    // Handle the exception
                    ((ProgressBar) view.findViewById(R.id.loadingView)).setVisibility(View.GONE);
                }
            });
            parser.execute(urlString);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        locationManager.removeUpdates(this);
        getNews();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
